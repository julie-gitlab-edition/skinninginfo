---
layout: master
description: main site of skinship
title: skinship | Home
---

<div class="welcome">
<h1>Welcome</h1>
<div class="section">
<p>This site is a collection of information related to osu! skinning. It is maintained by me, <a href="https://osu.ppy.sh/users/8388854">RockRoller</a>. The navigation bar at the top should lead you to everything you could want. If you encounter any problems with the site or any mistakes within its content please either notify me directly or go to the GitLab repository of this site and open an issue.</p>

<p>If you need any additional help with anything skinning related, feel free to ask on the forums or join the <a href="https://discord.skinship.xyz/">skinship discord server</a>. If you want to contribute to this site, please contact me beforehand. Just opening a pull request without contacting me first may lead to wasted time. I don't upload all work in progress articles.</p>
</div>

{% include ship.html %}

</div>

# Skinship Staff

<ul class="staff-grid">
  {% for staff in site.data.staff %}
    <li class="staff-item">
        <img class="avatar" src="https://a.ppy.sh/{{ staff.id }}">
        <div class="staff-description">
            <a href="https://osu.ppy.sh/users/{{ staff.id }}" class="name">{{ staff.name }}</a>
            <span class="role">{{ staff.role }}</span>
        </div>
    </li>
  {% endfor %}
</ul>

## Latest Updates

| Date       |                  Type                  |
| ---------- | :------------------------------------: |
| 25/10/2021 |           Tutorial: skin.ini           |
| 24/10/2021 |            Tutorial: Catch             |
| 29/06/2021 |        Guide: Centring Accuracy        |
| 29/06/2021 |        Guide: Centring Accuracy        |
| 27/03/2021 |          Guide: Mixing Skins           |
| 15/03/2021 |    Guide: How to import osu! skins     |
| 09/02/2021 |        Resource: Fringe Remover        |
| 20/01/2021 |          Tutorial: Animations          |
| 13/01/2021 | Resource: 21:9 Song Selection Template |
| 29/07/2020 |   Guide: Non-Pulsating Combo Numbers   |
